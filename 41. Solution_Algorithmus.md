### 41 求解算法

求解纳维斯托克斯方程需要解耦合的速度压力场。 现存几种求解算法能够将方程解耦，来分别计算速度和压力。一种预测-校正策略被用来解耦速度压力的计算。这种方法在许多数值方法的文献中被称为分离求解。\
除了分离求解的方法， 还有一种方法直接求解全耦合-也被称作块耦合-方程系统。一般来说，求解动量方程意味着需要求解三个速度分量方程以及一个由连续方程推导而来的压力方程。除了单独求解这四个离散方程， 全耦合方程组也可以直接被解出来。全耦合求解的方法提供了收敛率提升的可能性，然而，其也会导致更大的内存需求和收敛行为的改变。\
本节将讨论分离求解的方法，因为它是最常用的。 在最后一小节，我们将简要的讨论耦合法。\

#### 41.1 SIMPLE
![图 103: SIMPLE算法流程图](images/fig103.PNG)

图 103： SIMPLE算法流程图

图 103 展示了SIMPLE算法流程。 SIMPLE算法先预测速度然后修正压力和速度。这个过程将一直重复，直到满足收敛条件。图103中的标注来源于 $\text{simpleFoam}$ 求解器源码中的术语。 SIMPLE算法的具体求解步骤如下：
1. 检查是否收敛 —— $\text{simple.loop()}$
2. 使用动量预测器预测速度 —— $\text{UEqn.H}$
3. 修正压力和速度 —— $\text{pEqn.H}$
4. 根据湍流模型求解输运方程 —— $\text{turbulence->correct()}$
5. 返回步骤1

在OpenFOAM中，SIMPLE算法被用在稳态求解器中。

##### 41.1.1 预测器

$\text{simpleFoam}$ 中的预测器是一个动量预测器。


```
// 动量预测器
tmp <fvVectorMatrix > UEqn
( 
    fvm::div(phi , U)
  + turbulence ->divDevReff(U) 
  ==
    sources(U) 
); 
UEqn().relax(); 
sources.constrain(UEqn());
solve(UEqn() == -fvc::grad(p));
```

列表261：$\text{simpleFoam}$ 中 $\text{UEqn.H}$ 的预测器   

##### 4.1.2 修正器

通过带入预测速度来修正压力场被称为修正器。修正的压力又通过解连续性方程来修正速度。 非正交压力修正器循环仅在非正交网格中会被用到。


```
p.boundaryField ().updateCoeffs ();

volScalarField rAU (1.0/ UEqn().A()); 
U = rAU*UEqn().H(); 
UEqn.clear();

phi = fvc:: interpolate(U, "interpolate(HbyA)") & mesh.Sf(); 
adjustPhi(phi , U, p);

// 非正交压力修正器循环 
while (simple.correctNonOrthogonal ()) 
{
    fvScalarMatrix pEqn 
    (
        fvm:: laplacian(rAU , p) == fvc::div(phi)
    ); 
    pEqn.setReference(pRefCell , pRefValue);
    pEqn.solve();

    if (simple.finalNonOrthogonalIter ()) 
    {
       phi -= pEqn.flux(); 
    }
}

#include "continuityErrs.H"

// 动量修正器中的显示压力弛豫 
p.relax();

// 动量修正器
U -= rAU*fvc::grad(p); 
U.correctBoundaryConditions (); 
sources.correct(U);
```
列表 262：$\text{simpleFoam}$ 的 $\text{pEqn.H}$ 中的修正器

#### 41.2 PISO

PISO算法也应用了预测器-修正器策略。图 104 展示了PISO算法的流程图。用动量预测器来预测速度。接着压力和速度会被修正直到达到预设的迭代数。然后再解湍流模型的输运方程。

![图 104: PISO算法流程图](images/fig104.png)

图 104: PISO算法流程图

#### 41.3 PIMPLE

PIMPLE算法，一种结合了SIMPLE和PISO的算法，将在42节和42.2节进一步讨论。

#### 41.4 块耦合解法

块耦合方法是一种与此前提到的分离算法完全不同的方法。块耦合方法中的所有方程(三个速度方程和一个压力方程)同时求解。 这需要为所有四个变量构建一个完全耦合的离散方程系统。因此, 块耦合求解器的内存占用比顺序求解器要大得多。

##### 41.4.1 块耦合求解器

foam-extend项目 https://foam-extend.sourceforge.io/, 在写出时,分布有两个用块耦合求解方法的求解器.

$\textit{blockCoupledScalarTransportFoam}$

这个求解器从标准$\textit{scalarTransportFoam}$求解器衍生来的，用来求解两个耦合的无源标量的传输。

$$
\nabla\cdot(\mathbf{u}T) + \nabla\cdot(D_T\nabla T) = \alpha (T_s-T) \\
\nabla\cdot(D_{T_s}\nabla T_s) = \alpha(T-T_s)
$$

$\textit{pUCoupledFoam}$

该求解器是一个针对不可压缩的湍流单相流的稳态求解器。其能够同时求解速度和压力。

