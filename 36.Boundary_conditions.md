### 36 边界条件
当所分析问题的几何体被划分网格，边界补丁需要定义——即，用于界定几何体的面需要被指定。每个边界补丁属于某种类别。在36.1节中，将会讨论可能的类别。
#### 36.1 基本类型
##### 36.1.1 几何边界条件 
某些类型的边界补丁可以用纯几何来描述。这类补丁的数值处理方法本质上对于求解器是很清楚的，不需要过多的建模操作。  
**对称面边界**  如果一个问题具有对称性，就只需要对区域的一半进行建模。对称平面这类边界条件属于对称面类型。  
**空边界**  OpenFOAM通常生成三维网格。如果一个二维问题需要处理，在厚度方向上必须要有一个网格单元。平行于所考虑的面（二维面）的边界条件为空边界，用于将仿真处理为二维的。  
**楔形边界**  如果一个几何体是轴对称，那问题可以简化。在这个案例中，只需要对几何的一部分（楔形体）建模。这类边界条件被称为楔形。  
**循环边界**  循环的边界条件。  
**处理器类边界**  在区域分解过程中产生的子域之间的边界条件属于处理器类边界条件。  

##### 36.1.2 复杂的边界条件
有些类型的边界条件不只是区域的几何边界条件。比如，在壁面，通常会添加非滑移条件，因此，这些地方需要进一步建模。  
**Patch(补丁)**  这是适用于所有边界条件的普通类型的边界条件。如果没有下列类型的边界条件添加，那么就属于这类边界条件。  
**Wall(壁面)**  这是适用于壁面的特殊类型。在采用壁面模型做湍流建模时，必须要采用这类边界条件。  
patch和wall两类边界条件需要进一步定义。这些边界可以有原始的或衍生的类型的边界条件。

#### 36.2 原始边界条件
最重要的原始边界条件包括：  
**FixedValue**  数量的值被直接给出。  
**FixedGradient**  数量的梯度被直接给出。  
**zeroGradient**  数量的梯度被描绘为0。  
```
type	fixedValue ;
value	uniform (0 0 0) ;
```
列表236: 固定值边界条件

#### 36.3  衍生边界条件
衍生边界条件是由原始边界条件衍生出来的。这类边界条件可以被用于模拟更为复杂的情况。

##### 36.3.1 inletOutlet
inletOutlet边界条件的表现取决于流动方向。如果流动方向向外，采用zeroGradient边界条件。如果流动方向向内，采用固定值的边界条件（fixedValue）。流入的流速采用关键字inletvalue输入。流速需要给出，但不能是相对量。  
```
type inletOutlet ;
inletValue uniform (0 0 0) ;
value uniform (0 0 0) ;
```
列表237: inletOutlet 边界条件

##### 36.3.2  surfaceNormalFixedValue
surfaceNormalFixedValue边界条件用于描述向量场的范数。方向取自补丁面的曲面法向量。refValue为正表示这个量与曲面法向量有相同方向；为负值表示方向相反。
```
type surfaceNormalFixedValue ;
refValue uniform -0.1;
```
列表238: surfaceNormalFixedValue 边界条件

##### 36.3.3 pressureInletOutletVelocity
这个边界条件是pressureInletVelocity和inletOutlet的结合。

#### 36.4难点
##### 36.4.1 语法
当给边界条件设置fixedValue的时候，OpenFOAM需要在值的后面添加关键词uniform 或者nonuniform。  
列表239显示文件0/K。这里的入口边界条件的设置与列表236中不同。注意，这里没有关键词uniform。OpenFOAM的反应与关键字版本后的值不同。  
列表240显示OpenFOAM中的警告信息，当如同列表239一样，关键词版本后的数值是2.0时。在这个例子中，OpenFOAM假定为uniform。  
如果在关键词版本后面的数值为2.1，OpenFOAM会给出错误提示，如列表241所示。  
在所有例子中，使用OpenFOAM-2.1.X版本。作者确认版本2.0与2.1的区别的原因在于可能的边界条件拓展。可参考OpenFOAM-2.1.0的版本说明。（http://www.openfoam.org /version2.1.0/boundary-conditions.php）
```
FoamFile
{
	version		2.0;
	format		ascii ;
	class		volScalarField ;
	object		k ;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //
dimensions		[0 2 -2 0 0 0 0];
internalField	uniform 1e-8;
boundaryField
{
	inlet
	{
		type	fixedValue ;
		value	1e-8;
	}
```
列表239: 文件 0/k

```
--> FOAM Warning :
From function Field < Type >:: Field ( const word & keyword , const dictionary & , const label )
in file / home / user / OpenFOAM / OpenFOAM -2.1. x / src / OpenFOAM / lnInclude / Field .C at line 262
Reading "/ home / user / OpenFOAM / user -2.1. x / run / twoPhaseEulerFoam / bubblePlume / case /0/ k ::
boundaryField :: inlet " from line 25 to line 26
expected keyword ’ uniform ’ or ’ nonuniform ’ , assuming deprecated Field format from Foam
version 2.0
```
列表240：警告信息：漏掉关键词

```
--> FOAM FATAL IO ERROR :
expected keyword ’ uniform ’ or ’ nonuniform ’ , found on line 26 the doubleScalar 1e -08
file : / home / user / OpenFOAM / user -2.1. x/ run / twoPhaseEulerFoam / bubblePlume / case /0/ k :: boundaryField
:: inlet from line 25 to line 26.
From function Field < Type >:: Field ( const word & keyword , const dictionary & , const label )
in file / home / user / OpenFOAM / OpenFOAM -2.1. x / src / OpenFOAM / lnInclude / Field .C at line 278.
FOAM exiting
```
列表241: 警告信息：漏掉关键词

#### 36.5 时变的边界条件
时变的边界条件可以帮助避免求解域无用的初始化。最简单的初始化是将计算域内的所有量都设为0，请参见第29章中的列表161。  
在仿真的初始阶段，当非0值的某些边界条件遇到相邻单元的0值时，由于大的相对速度，可能会出现计算稳定性问题。一个求解可以在计算初始阶段选用很小的时间步长。另外的求解需要确定时变边界条件。这样，在边界处的场量初始为很小，在一定的时间跨度内增长到它最终的值。

##### 36.5.1  uniformFixedValue
这个边界条件为fixedValue边界条件的一般化形式。请参见http://www.openfoam.org /version2.1.0/boundary-conditons.php  
列表242示范采用一个确定值来定义时变边界条件。在时间t=0.0s和t=5.0s之间，边界条件的值通过在间隔两端的值之间做线性插值得到。在此间隔结束后，边界条件的值保持不变。
```
inlet
{
	type			uniformFixedValue ;
	uniformValue	table
	(
		( 0.0	(0.0 0.0 0.0) )
		( 5.0	(0.0 0.0 0.1) )
	);
}
```
列表242: 定义时变的边界条件

**难点：老的两相求解器**  
该边界条件在OpenFOAM老版本的两相求解器中不可用。在OpenFOAM-4中使用时变边界条件不会有问题。